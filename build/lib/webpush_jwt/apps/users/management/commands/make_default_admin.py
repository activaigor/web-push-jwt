from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Prepare a default admin user'

    def handle(self, *args, **options):
        assert settings.DEFAULT_ADMIN_USER is not None, "You need to set DEFAULT_ADMIN_USER"
        assert settings.DEFAULT_ADMIN_PASS is not None, "You need to set DEFAULT_ADMIN_PASS"
        from webpush_jwt.apps.users.models import CustomUser
        if not CustomUser.objects.filter(email=settings.DEFAULT_ADMIN_USER).exists():
            CustomUser.objects.create_superuser(email=settings.DEFAULT_ADMIN_USER,
                                                password=settings.DEFAULT_ADMIN_PASS)
            print('Default admin user prepared...')
