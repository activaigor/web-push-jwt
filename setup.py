import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="webpush-jwt",
    version="0.2.6",
    author="Igor Rizhyi",
    author_email="igorrizhyi@gmail.com",
    description="A small lib for JWT manipulations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'PyJWT==1.7.1',
        'Django>=2.1.15',
        'djangorestframework>=3.10.3'
    ]
)
