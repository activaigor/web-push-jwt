import jwt
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from webpush_jwt.lib.builder import JWTRefBuilder
from webpush_jwt.lib.instances import JWTRefInstance


class JWTRefAuthentication(TokenAuthentication):

    keyword = 'Bearer'

    def authenticate_credentials(self, token):
        jwt_ref = self._parse_token(token)
        self._check_jwt_ref(jwt_ref)
        return jwt_ref, token

    @staticmethod
    def _parse_token(token) -> JWTRefInstance:
        try:
            return JWTRefBuilder.decode(token)
        except jwt.ExpiredSignature:
            raise AuthenticationFailed('Signature has expired')
        except jwt.InvalidTokenError:
            raise AuthenticationFailed('Invalid token')

    @staticmethod
    def _check_jwt_ref(jwt_ref: JWTRefInstance) -> None:
        if jwt_ref.consumer_type not in JWTRefInstance.USER_TYPES:
            raise AuthenticationFailed(f"Invalid type of consumer ({jwt_ref.consumer_type})")
        if not jwt_ref.consumer_id:
            raise AuthenticationFailed('There is no consumer_id in payload')
